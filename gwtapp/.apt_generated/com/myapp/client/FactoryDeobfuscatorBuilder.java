// Automatically Generated -- DO NOT EDIT
// com.myapp.client.Factory
package com.myapp.client;
import java.util.Arrays;
import com.google.web.bindery.requestfactory.vm.impl.OperationData;
import com.google.web.bindery.requestfactory.vm.impl.OperationKey;
import com.google.gwt.core.shared.GwtIncompatible;
@GwtIncompatible("Server-side only but loaded through naming convention so must be in same package as shared Factory interface")
public final class FactoryDeobfuscatorBuilder extends com.google.web.bindery.requestfactory.vm.impl.Deobfuscator.Builder {
{
withOperation(new OperationKey("cu4TZURJbtqwbD8H7hNQRBvuPUg="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/InstanceRequest;")
  .withDomainMethodDescriptor("()V")
  .withMethodName("remove")
  .withRequestContext("com.myapp.client.Factory$EmployeeRequest")
  .build());
withOperation(new OperationKey("$q462IgBAFLSDcO16ayo71XFAOg="),
  new OperationData.Builder()
  .withClientMethodDescriptor("(Ljava/lang/Long;)Lcom/google/web/bindery/requestfactory/shared/Request;")
  .withDomainMethodDescriptor("(Ljava/lang/Long;)Lcom/myapp/server/Employee;")
  .withMethodName("find")
  .withRequestContext("com.myapp.client.Factory$EmployeeRequest")
  .build());
withOperation(new OperationKey("NjICAbebn61cgMMyy5ftIqIVQdA="),
  new OperationData.Builder()
  .withClientMethodDescriptor("()Lcom/google/web/bindery/requestfactory/shared/InstanceRequest;")
  .withDomainMethodDescriptor("()V")
  .withMethodName("persist")
  .withRequestContext("com.myapp.client.Factory$EmployeeRequest")
  .build());
withRawTypeToken("x88p3$HXNrNvh6V3xdI8bDMRA5I=", "com.myapp.client.EmployeeProxy");
withRawTypeToken("w1Qg$YHpDaNcHrR5HZ$23y518nA=", "com.google.web.bindery.requestfactory.shared.EntityProxy");
withRawTypeToken("FXHD5YU0TiUl3uBaepdkYaowx9k=", "com.google.web.bindery.requestfactory.shared.BaseProxy");
withClientToDomainMappings("com.myapp.server.Employee", Arrays.asList("com.myapp.client.EmployeeProxy"));
}}
