package com.myapp.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.myapp.client.Factory.EmployeeRequest;


public class viewOne extends Composite{
	
	private VerticalPanel vPanel = new VerticalPanel();
	private TextBox txt1;
	private Label myLabel;
	Factory factory;
	public viewOne () {
	factory = GWT.create(Factory.class);
	factory.initialize(new SimpleEventBus());
	initWidget(this.vPanel);
	
	vPanel.setBorderWidth(1);
	
	this.myLabel = new Label("Name");
	vPanel.add(this.myLabel);
	
	HorizontalPanel hPanel = new HorizontalPanel();
	hPanel.setBorderWidth(1);
	
	this.txt1 = new TextBox();
	this.txt1.setText("name");
	hPanel.add(this.txt1);
	
	Button buton1 = new Button("Add");
	buton1.addClickHandler(new ButtonClickHandler());
	hPanel.add(buton1);
	
	vPanel.add(hPanel);
	
	}
	private class ButtonClickHandler implements ClickHandler{
		
		public void onClick(ClickEvent event) {
			String theText = txt1.getText();
			myLabel.setText(theText);

				EmployeeRequest context = factory.createEmployeeRequest();
				EmployeeProxy contact = context.create(EmployeeProxy.class);
				contact.setName("Anna");		
				context.persist().using(contact).fire();

		}
	}
	

}
