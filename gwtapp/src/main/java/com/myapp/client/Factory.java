package com.myapp.client;


import com.google.web.bindery.requestfactory.shared.InstanceRequest;
import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.RequestFactory;
import com.google.web.bindery.requestfactory.shared.Service;
import com.myapp.server.Employee;

public interface Factory extends RequestFactory{
	EmployeeRequest createEmployeeRequest ();
	@Service(value = Employee.class)
	public interface EmployeeRequest extends RequestContext{
		Request<EmployeeProxy> find (Long id);
		//Request<List<EmployeeProxy>> findAllEmployee ();
		InstanceRequest<EmployeeProxy, Void> persist ();
		InstanceRequest<EmployeeProxy, Void> remove ();
	}
}
