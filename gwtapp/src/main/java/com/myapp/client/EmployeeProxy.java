package com.myapp.client;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyFor;
import com.myapp.server.Employee;

@ProxyFor(value = Employee.class)
public interface EmployeeProxy extends EntityProxy {
	Long getId ();
	String getName ();
	void setName (String text);

}
