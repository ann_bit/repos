package com.myapp.server;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;



@Repository("employeeDao")
public class EmployeeDaoImpl extends AbstractDao 
//implements EmployeeDao
{
 
    public void saveEmployee(Employee employee) {
        persist(employee);
    }
 
    @SuppressWarnings("unchecked")
    public List<Employee> findAllEmployees() {
        Criteria criteria = getSession().createCriteria(Employee.class);
        return (List<Employee>) criteria.list();
    }
 
    public void deleteEmployeeById(Long id) {
        Query query = getSession().createSQLQuery("delete from Employee where id = :id");
        query.setLong("id", id);
        query.executeUpdate();
    }
 
     
    public Employee findById(Long id){
        Criteria criteria = getSession().createCriteria(Employee.class);
        criteria.add(Restrictions.eq("id", id));
        return (Employee) criteria.uniqueResult();
    }
     
    public void updateEmployee(Employee employee){
        getSession().update(employee);
    }
     
}