package com.myapp.server;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import com.myapp.spring.configuration.AppConfig;

@Entity
@Table(name="EMPLOYEE")
public class Employee {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
	@Column(name = "VERSION")
	private Integer version;
	@Column(name = "NAME", nullable = false)
    private String name;
    
	public Employee(){}
	
	public static Employee findEmployee (Long id) { 
		// analogicznie do metody w main
	AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    EmployeeService service = (EmployeeService) context.getBean("employeeService");
    Employee employee = service.findById(id);
    context.close();
    return employee; }
	public Long getId () {
		return id;
		}
	public Integer getVersion () {
		return version;
		}
	public void setVersion (Integer version) {
		this.version=version;
		}
	public String getName() {
			return name;
		}
	public void setName(String name) {
			this.name = name;
		}
	//public static List<Employee> findAllContacts() { return CEM.list(); };
	public static Employee find (Long id) {
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
	    EmployeeService service = (EmployeeService) context.getBean("employeeService");
	    Employee employee = service.findById(id);
	    context.close();
	    return employee;}
	public void persist() {
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		EmployeeService service = (EmployeeService) context.getBean("employeeService");
		service.saveEmployee(this);
		context.close();}
	public void remove() {
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		EmployeeService service = (EmployeeService) context.getBean("employeeService");
		service.deleteEmployeeById(this.getId());
		context.close();}
}
