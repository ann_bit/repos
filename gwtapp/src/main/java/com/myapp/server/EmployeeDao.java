package com.myapp.server;

import java.util.List;


 
public interface EmployeeDao {
 
    void saveEmployee(Employee employee);
     
    List<Employee> findAllEmployees();
     
    void deleteEmployeeById(Long id);
     
    Employee findById(Long id);
     
    void updateEmployee(Employee employee);
}