package com.myapp.main;


import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.myapp.server.Employee;
import com.myapp.server.EmployeeService;
import com.myapp.spring.configuration.AppConfig;
 
public class AppMain {
 
    public static void main(String args[]) {
         AbstractApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
         EmployeeService service = (EmployeeService) context.getBean("employeeService");
 

        Employee employee1 = new Employee();
        employee1.setName("Jan Kowalski");
        employee1.setVersion(1);

        service.saveEmployee(employee1);


        List<Employee> employees = service.findAllEmployees();
        for (Employee emp : employees) {
            System.out.println(emp);
        }
 

        service.deleteEmployeeById((long) 1);

        Employee employee = service.findById((long) 1);
        service.updateEmployee(employee);

        List<Employee> employeeList = service.findAllEmployees();
        for (Employee emp : employeeList) {
            System.out.println(emp);
        }
 
        context.close();
    }
}